export const setItemToLs = (name: string, value: {} | string) => {
    localStorage.setItem(name, JSON.stringify(value))
}

export const getItemFromLs = (name: string) => {
    try {
        //@ts-ignore
        return JSON.parse(localStorage.getItem(name))
    }
    catch {
        return localStorage.getItem(name)
    }

}

export const removeItemFromLs = (name: string) => {
    localStorage.removeItem(name)
}