import paint from './paint.png'
import comment from './comment.png'
import trash from './trash.png'

export const images = {
    paint,
    comment,
    trash,
}