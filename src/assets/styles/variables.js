export const variables = {
    //colors
    defaultBlack: '#292929',
    theme: '#fffef9',
    defaultGrey: '#e0e0e0',
    lightGrey: '#8f8f8f',
    dangerRed: '#e82e2e',
    defaultYellow: '#fff03c',

    //sizing
    textNormal: '24px',
    defaultPadding: '33px',
}