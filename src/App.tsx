import React, {ChangeEvent, useEffect, useState} from 'react'
import './assets/styles/default.scss'
import {initialState} from './storage/initialState'
import {Board} from './components/Board/Board'
import {getItemFromLs, setItemToLs} from './helpers/localStorageHandler'
import {Popup} from './ui/Popup'
import {useSwitcher} from './hooks/useSwitcher'
import {
  DEFAULT_AUTHOR_NAME,
  DEFAULT_DESCRIPTION,
  EMPTY_STRING,
  LOCAL_STORAGE_AUTHOR,
  LOCAL_STORAGE_STATE
} from './constants/constants'

function App() {
  const [state, setState] = useState(getItemFromLs(LOCAL_STORAGE_STATE) ?? initialState)
  const [author, setAuthor] = useState(getItemFromLs(LOCAL_STORAGE_AUTHOR) || DEFAULT_AUTHOR_NAME)

  //SHOW PREVIEW TASK INPUT
  const showPreviewTaskInput = (columnIndex: number) => {
    const cloneState = [...state]
    cloneState[columnIndex].showInput = true
    setState(cloneState)
    setItemToLs(LOCAL_STORAGE_STATE, state)
  }

  //ONCHANGE TASK PREVIEW
  const onChangeTaskPreview = (e: ChangeEvent<HTMLInputElement>, index: number) => {
    const cloneState = [...state]
    cloneState[index].input = e.target.value
    setState(cloneState)
    setItemToLs(LOCAL_STORAGE_STATE, state)
  }

  //ADD TASK
  const addTask = (inputValue: string, index: number) => {
    const cloneState = [...state]
    if (inputValue !== '') {
      const newTask = {
        author: EMPTY_STRING,
        title: EMPTY_STRING,
        desc: DEFAULT_DESCRIPTION,
        comments: [],
        input: EMPTY_STRING,
        showInput: false,
      }
      newTask.title = inputValue
      newTask.author = author
      cloneState[index].todos.push(newTask)
      cloneState[index].input = EMPTY_STRING
    }
    setState(cloneState)
    cloneState[index].showInput = false
    setItemToLs(LOCAL_STORAGE_STATE, state)
  }

  //REMOVE TASK
  const removeTask = (columnIndex: number, taskIndex: number) => {
    const cloneState = [...state]
    cloneState[columnIndex].todos.splice(taskIndex, 1)
    setState(cloneState)
    setItemToLs(LOCAL_STORAGE_STATE, state)
  }

  //POPUP
  const showPopup = useSwitcher()
  const [popupValue, setPopupValue] = useState(EMPTY_STRING)
  useEffect(() => {
    if (getItemFromLs(LOCAL_STORAGE_AUTHOR) === null) {
      showPopup.on()
    }
  }, [showPopup])
  const popupInputHandler = (e: ChangeEvent<HTMLInputElement>) => {
    setPopupValue(e.target.value)
  }
  const saveNameHandler = () => {
    setItemToLs(LOCAL_STORAGE_AUTHOR, popupValue)
    setAuthor(popupValue)
    showPopup.off()
  }

  //CHANGE STATUS
  function changeTaskStatus(e: ChangeEvent<HTMLSelectElement>, taskIndex: number, columnIndex: number) {
    const cloneState = [...state]
    const selectedTask = cloneState[columnIndex].todos.splice(taskIndex, 1)
    cloneState[Number(e.target.value)].todos.push(selectedTask[0])
    setState(cloneState)
    setItemToLs(LOCAL_STORAGE_STATE, state)
  }

  //ONCHANGE DESCRIPTION
  const onChangeTaskDesc = (e: ChangeEvent<HTMLTextAreaElement>, taskIndex: number, columnIndex: number) => {
    const cloneState = [...state]
    cloneState[columnIndex].todos[taskIndex].desc = e.target.value
    setState(cloneState)
    setItemToLs(LOCAL_STORAGE_STATE, state)
  }

  //DESCRIPTION VALUE HANDLER
  const descriptionValueHandler = (columnIndex: number, taskIndex: number) => {
    const cloneState = [...state]
    const descriptionValueSwitcher = (newValue: string) => {
      cloneState[columnIndex].todos[taskIndex].desc = newValue
      setState(cloneState)
      setItemToLs(LOCAL_STORAGE_STATE, state)
    }
    if (cloneState[columnIndex].todos[taskIndex].desc === DEFAULT_DESCRIPTION) {
      descriptionValueSwitcher(EMPTY_STRING)
      return
    }
    if (cloneState[columnIndex].todos[taskIndex].desc === EMPTY_STRING) {
      descriptionValueSwitcher(DEFAULT_DESCRIPTION)
      return
    }
  }

  //ONCHANGE COMMENT
  const onChangeComment = (e: ChangeEvent<HTMLInputElement>, taskIndex: number, columnIndex: number) => {
    const cloneState = [...state]
    cloneState[columnIndex].todos[taskIndex].input = e.target.value
    setState(cloneState)
    setItemToLs(LOCAL_STORAGE_STATE, state)
  }

  //ADD COMMENT
  const addComment = (inputValue: string, columnIndex: number, taskIndex: number) => {
    const cloneState = [...state]
    if (inputValue !== EMPTY_STRING) {
      const newComment = {
        author: EMPTY_STRING,
        body: EMPTY_STRING,
      }
      newComment.body = inputValue
      newComment.author = author
      cloneState[columnIndex].todos[taskIndex].comments.push(newComment)
      cloneState[columnIndex].todos[taskIndex].input = EMPTY_STRING
    }
    cloneState[columnIndex].todos[taskIndex].showInput = false
    setState(cloneState)
    setItemToLs(LOCAL_STORAGE_STATE, state)
  }

  //REMOVE COMMENT
  const removeComment = (columnIndex: number, taskIndex: number, commentIndex: number) => {
    const cloneState = [...state]
    cloneState[columnIndex].todos[taskIndex].comments.splice(commentIndex, 1)
    setState(cloneState)
    setItemToLs(LOCAL_STORAGE_STATE, state)
  }

  //SHOW COMMENT
  const showComment = (columnIndex: number, taskIndex: number) => {
    const cloneState = [...state]
    cloneState[columnIndex].todos[taskIndex].showInput = true
    setState(cloneState)
    setItemToLs(LOCAL_STORAGE_STATE, state)
  }

  //ON EDIT COMMENT
  const onEditComment = (e: ChangeEvent<HTMLTextAreaElement>, columnIndex: number, taskIndex: number, commentIndex: number) => {
    const cloneState = [...state]
    cloneState[columnIndex].todos[taskIndex].comments[commentIndex].body = e.target.value
    setState(cloneState)
    setItemToLs(LOCAL_STORAGE_STATE, state)
  }

  return (
    <>
      {showPopup.isOn
        && <Popup
          popupClose={() => showPopup.off()}
          children={
            <>
              <input
                  type="text"
                  onChange={(e) => popupInputHandler(e)}
                  value={popupValue}
                  placeholder="Введите ваше имя"
              />
              <button onClick={saveNameHandler}>OK</button>
            </>
          }
      />}
      <Board state={state}
             author={author}
             addTask={addTask}
             removeTask={removeTask}
             onChangeTaskPreview={onChangeTaskPreview}
             showPreviewTaskInput={showPreviewTaskInput}
             changeTaskStatus={changeTaskStatus}
             onChangeTaskDesc={onChangeTaskDesc}
             descriptionValueHandler={descriptionValueHandler}
             onChangeComment={onChangeComment}
             addComment={addComment}
             removeComment={removeComment}
             showComment={showComment}
             onEditComment={onEditComment} />
    </>
  )
}

export default App
