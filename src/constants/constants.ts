export const TO_DO = 'TODO'
export const IN_PROGRESS = 'In progress'
export const TESTING = 'Testing'
export const DONE = 'Done'

export const EMPTY_STRING = ''
export const DEFAULT_DESCRIPTION = 'Click here to add description...'
export const DEFAULT_AUTHOR_NAME = 'user'

export const LOCAL_STORAGE_STATE = 'state'
export const LOCAL_STORAGE_AUTHOR = 'author'