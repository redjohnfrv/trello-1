import React, {ChangeEvent} from 'react'
import {Container} from '../../ui/Container'
import {Column} from '../Column/Column'
import {BoardTitle} from './BoardTitle'
import styled from 'styled-components'
import {StateType} from '../../storage/initialState'

type ColumnsType = {
    state: StateType[]
    author: string
    addTask: (inputValue: string, index: number) => void
    removeTask: (columnIndex: number, taskIndex: number) => void
    onChangeTaskPreview: (e: ChangeEvent<HTMLInputElement>, index: number) => void
    showPreviewTaskInput: (columnIndex: number) => void
    changeTaskStatus: (e: ChangeEvent<HTMLSelectElement>, taskIndex: number, columnIndex: number) => void
    onChangeTaskDesc: (e: ChangeEvent<HTMLTextAreaElement>, taskIndex: number, columnIndex: number) => void
    onChangeComment: (e: ChangeEvent<HTMLInputElement>, taskIndex: number, columnIndex: number) => void
    addComment: (inputValue: string, columnIndex: number, taskIndex: number) => void
    removeComment: (columnIndex: number, taskIndex: number, commentIndex: number) => void
    showComment: (columnIndex: number, taskIndex: number) => void
    onEditComment: (e: ChangeEvent<HTMLTextAreaElement>, columnIndex: number, taskIndex: number, commentIndex: number) => void
    descriptionValueHandler: (columnIndex: number, taskIndex: number) => void
}

export function Board({
    state,
    author,
    addTask,
    removeTask,
    onChangeTaskPreview,
    showPreviewTaskInput,
    changeTaskStatus,
    onChangeTaskDesc,
    onChangeComment,
    addComment,
    removeComment,
    showComment,
    onEditComment,
    descriptionValueHandler,}: ColumnsType) {

    return (
        <Container>
            <BoardTitle author={author} />
            <ColumnWrapper>
                {state.map( (item: StateType, index: number)  => {
                    return <Column
                        key={index}
                        data={item}
                        addTask={addTask}
                        columnIndex={index}
                        removeTask={removeTask}
                        columnInputValue={item.input}
                        onChangeTaskPreview={onChangeTaskPreview}
                        showPreviewTaskInput={showPreviewTaskInput}
                        changeTaskStatus={changeTaskStatus}
                        onChangeTaskDesc={onChangeTaskDesc}
                        onChangeComment={onChangeComment}
                        addComment={addComment}
                        removeComment={removeComment}
                        showComment={showComment}
                        onEditComment={onEditComment}
                        descriptionValueHandler={descriptionValueHandler} />
                })}
            </ColumnWrapper>
        </Container>
    )
}

const ColumnWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`