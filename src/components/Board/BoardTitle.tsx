import React from 'react'
import styled from 'styled-components'
import {images} from '../../assets/images/images'

type BoardTitleProps = {
    author: string
}

export function BoardTitle({author}: BoardTitleProps) {
    return (
        <BoardTitleWrapper>
            <h1>Hello, {author}!</h1>
        </BoardTitleWrapper>
    )
}

const BoardTitleWrapper = styled.div`
  width: 100%;
  max-width: 200px;
  margin-bottom: 36px;
  background: url(${images.paint}) center/cover no-repeat;
`