import React, {ChangeEvent, useCallback, useEffect} from 'react'
import styled from 'styled-components'
import {images} from '../../assets/images/images'
import {Popup} from '../../ui/Popup'
import {useSwitcher} from '../../hooks/useSwitcher'
import {TodoType} from '../../storage/initialState'
import {Task} from './Task'

type TaskProps = {
    task: TodoType
    taskIndex: number
    removeTask: (columnIndex: number, taskIndex: number) => void
    columnIndex: number
    changeTaskStatus: (e: ChangeEvent<HTMLSelectElement>, taskIndex: number, columnIndex: number) => void
    onChangeTaskDesc: (e: ChangeEvent<HTMLTextAreaElement>, taskIndex: number, columnIndex: number) => void
    onChangeComment: (e: ChangeEvent<HTMLInputElement>, taskIndex: number, columnIndex: number) => void
    addComment: (inputValue: string, columnIndex: number, taskIndex: number) => void
    removeComment: (columnIndex: number, taskIndex: number, commentIndex: number) => void
    showComment: (columnIndex: number, taskIndex: number) => void
    onEditComment: (e: ChangeEvent<HTMLTextAreaElement>, columnIndex: number, taskIndex: number, commentIndex: number) => void
    descriptionValueHandler: (columnIndex: number, taskIndex: number) => void
}

export function TaskPreview({
    task,
    taskIndex,
    removeTask,
    columnIndex,
    changeTaskStatus,
    onChangeTaskDesc,
    onChangeComment,
    addComment,
    removeComment,
    showComment,
    onEditComment,
    descriptionValueHandler,}: TaskProps) {

    const showPopup = useSwitcher()
    const closePopupOnEsc = useCallback((e: KeyboardEvent) => {
        const {key} = e
        if (key === 'Escape') showPopup.off()
    }, [showPopup])
    useEffect(() => {
        if (showPopup.isOn) document.addEventListener('keydown', closePopupOnEsc)
        return () => document.removeEventListener('keydown', closePopupOnEsc)
    }, [closePopupOnEsc, showPopup.isOn])

    return (
        <>
            {showPopup.isOn && <Popup
                popupClose={() => showPopup.off()}
                children={
                    <Task
                        task={task}
                        columnIndex={columnIndex}
                        changeTaskStatus={changeTaskStatus}
                        taskIndex={taskIndex}
                        onChangeTaskDesc={onChangeTaskDesc}
                        onChangeComment={onChangeComment}
                        addComment={addComment}
                        removeComment={removeComment}
                        showComment={showComment}
                        onEditComment={onEditComment}
                        closePopup={showPopup}
                        descriptionValueHandler={descriptionValueHandler} />
                }
            />}
            <TaskPreviewWrapper onClick={() => showPopup.on()}>
                <span>{task.title}</span>
                <TaskRemoveButton onClick={() => removeTask(columnIndex, taskIndex)} />
                <TaskCommentsCounterWrapper>
                    {task.comments.length}
                </TaskCommentsCounterWrapper>
            </TaskPreviewWrapper>
        </>
    )
}

const TaskPreviewWrapper = styled.div`
  position: relative;
  box-sizing: border-box;
  width: calc(100% - 8px);
  height: 56px;
  margin-bottom: 12px;
  padding: 0 16px 0 8px;
  line-height: 36px;
  text-align: left;
  background: #fff;
  border-radius: 15px;
  cursor: pointer;
  
  & span {
    display: inline-block;
    width: 140px;
    font-weight: bold;
    white-space: nowrap;
    font-weight: bold;
    overflow: auto;
    text-overflow: ellipsis;
  }
`

const TaskRemoveButton = styled.div`
  position: absolute;
  top: 20px;
  left: calc(100% + 8px);
  width: 20px;
  height: 20px;
  background: url(${images.trash}) center / cover no-repeat;
  cursor: grab;
  
  &:hover {
    filter: blur(.5px);
  }
`

const TaskCommentsCounterWrapper = styled.div`
  position: absolute;
  top: 24px;
  left: 28px;
  font-size: 12px;
  
  &:before {
    content: '';
    position: absolute;
    top: 10px;
    left: -20px;
    width: 15px;
    height: 15px;
    background: url(${images.comment}) center / cover no-repeat;
  }
`