import React, {ChangeEvent} from 'react'
import styled from 'styled-components'
import {variables} from '../../assets/styles/variables'
import {DONE, IN_PROGRESS, TESTING, TO_DO} from '../../constants/constants'
import {TodoType} from '../../storage/initialState'
import {TaskInput} from './TaskInput'
import {useSwitcher, UseSwitcherType} from '../../hooks/useSwitcher'
import {TaskComments} from './TaskComments'

enum EStatus {
    'TODO',
    'In progress',
    'Testing',
    'Done',
}

type TaskProps = {
    task: TodoType
    columnIndex: number
    taskIndex: number
    changeTaskStatus: (e: ChangeEvent<HTMLSelectElement>, taskIndex: number, columnIndex: number) => void
    onChangeTaskDesc: (e: ChangeEvent<HTMLTextAreaElement>, taskIndex: number, columnIndex: number) => void
    onChangeComment: (e: ChangeEvent<HTMLInputElement>, taskIndex: number, columnIndex: number) => void
    addComment: (inputValue: string, columnIndex: number, taskIndex: number) => void
    removeComment: (columnIndex: number, taskIndex: number, commentIndex: number) => void
    showComment: (columnIndex: number, taskIndex: number) => void
    onEditComment: (e: ChangeEvent<HTMLTextAreaElement>, columnIndex: number, taskIndex: number, commentIndex: number) => void
    closePopup: UseSwitcherType
    descriptionValueHandler: (columnIndex: number, taskIndex: number) => void
}

export function Task({
    task,
    columnIndex,
    taskIndex,
    changeTaskStatus,
    onChangeTaskDesc,
    onChangeComment,
    addComment,
    removeComment,
    showComment,
    onEditComment,
    closePopup,
    descriptionValueHandler,}: TaskProps) {

    const showTextArea = useSwitcher()

    return (
        <TaskWrapper>
            <TaskTitleWrapper>
                <h1>{task.title}</h1>
                <span>(Автор: {task.author})</span>
            </TaskTitleWrapper>
            <TaskBodyWrapper>
                <TaskDescriptionWrapper>
                    <h2>description</h2>
                    <TaskDescriptionBody>
                        <span onClick={() => {
                            descriptionValueHandler(columnIndex, taskIndex)
                            showTextArea.on()}
                        }>{task.desc}</span>
                        {showTextArea.isOn
                            && <textarea
                                autoFocus={true}
                                value={task.desc}
                                onChange={(e) => onChangeTaskDesc(e, taskIndex, columnIndex)}
                                onBlur={() => {
                                    descriptionValueHandler(columnIndex, taskIndex)
                                    showTextArea.off()
                                }} />}
                    </TaskDescriptionBody>
                </TaskDescriptionWrapper>
                <TaskStatusWrapper>
                    <select
                        value={columnIndex}
                        onChange={(e) => {
                            closePopup.off()
                            changeTaskStatus(e, taskIndex, columnIndex)
                        }}>
                        <option value={EStatus[TO_DO]}>{TO_DO}</option>
                        <option value={EStatus[IN_PROGRESS]}>{IN_PROGRESS}</option>
                        <option value={EStatus[TESTING]}>{TESTING}</option>
                        <option value={EStatus[DONE]}>{DONE}</option>
                    </select>
                </TaskStatusWrapper>
            </TaskBodyWrapper>
            <TaskCommentsWrapper>
                <h2>comments</h2>
                <TaskComments
                    comments={task.comments}
                    columnIndex={columnIndex}
                    taskIndex={taskIndex}
                    removeComment={removeComment}
                    onEditComment={onEditComment} />
                {task.showInput
                    && <TaskInput
                        taskIndex={taskIndex}
                        columnIndex={columnIndex}
                        commentInputValue={task.input}
                        onChangeComment={onChangeComment}
                        addComment={addComment} />}
                <span
                    onClick={() =>showComment(columnIndex, taskIndex)}
                ><span>+</span> Add comment</span>
            </TaskCommentsWrapper>
        </TaskWrapper>
    )
}

const TaskWrapper = styled.div`
  
`

const TaskTitleWrapper = styled.div`
  display: flex;
  justify-content: left;
  align-items: center;
  width: max-content;
  margin-bottom: 36px;
  
  & span {
    margin-left: 8px;
    white-space: nowrap;
    font-size: 24px;
  }
`

const TaskBodyWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 24px;
`

const TaskDescriptionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 70%;
  
  & h2 {
    color: ${variables.lightGrey};
    text-transform: uppercase;
    font-size: 16px;
    margin-bottom: 0;
  }
`
const TaskStatusWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 30%;
`

const TaskCommentsWrapper = styled.div`
  text-align: left;
  
  & h2 {
    color: ${variables.lightGrey};
    text-transform: uppercase;
    font-size: 16px;
    margin-bottom: 0;
  }
  
  & span {
    font-size: ${variables.textNormal};
    color: ${variables.defaultBlack};
    -webkit-text-stroke: unset;
    cursor: pointer;
    
    & span {
      position: relative;
      top: 2px;
      font-size: 36px;
      font-weight: bold;
      color: ${variables.defaultYellow};
      -webkit-text-stroke: .5px ${variables.defaultBlack};
    }
  }
`
const TaskDescriptionBody = styled.div`
  position: relative;
  
  & span {
    width: 350px;
    padding-left: 8px;
    text-align: left;
    cursor: pointer;
  }
  
  & textarea {
    position: absolute;
    left: 0;
    top: 0;
    width: 350px;
    height: 60px;
    overflow: scroll;
  }
`