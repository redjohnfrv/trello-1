import React, {ChangeEvent} from 'react'
import styled from 'styled-components'
import {Comment} from '../Comment/Comment'
import {CommentType} from '../../storage/initialState'

type TaskCommentsProps = {
    comments: CommentType[]
    columnIndex: number
    taskIndex: number
    removeComment: (columnIndex: number, taskIndex: number, commentIndex: number) => void
    onEditComment: (e: ChangeEvent<HTMLTextAreaElement>, columnIndex: number, taskIndex: number, commentIndex: number) => void
}

export function TaskComments({
    comments,
    columnIndex,
    taskIndex,
    removeComment,
    onEditComment,}: TaskCommentsProps) {
    return (
        <TaskCommentsListWrapper>
            {comments.map((item, index) => {
                return <Comment
                    key={index}
                    columnIndex={columnIndex}
                    taskIndex={taskIndex}
                    commentIndex={index}
                    commentAuthor={item.author}
                    commentBody={item.body}
                    removeComment={removeComment}
                    onEditComment={onEditComment} />
            })}
        </TaskCommentsListWrapper>
    )
}

const TaskCommentsListWrapper = styled.div`

`