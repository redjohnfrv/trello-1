import React from 'react'
import {CommentInputEventChangeType, InputAddComment} from '../../ui/Input'

export function TaskInput({
    taskIndex,
    columnIndex,
    commentInputValue,
    onChangeComment,
    addComment,}: CommentInputEventChangeType) {
    return (
        <InputAddComment
            taskIndex={taskIndex}
            columnIndex={columnIndex}
            commentInputValue={commentInputValue}
            onChangeComment={onChangeComment}
            addComment={addComment} />
    )
}