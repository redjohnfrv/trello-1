import React, {ChangeEvent} from 'react'
import styled from 'styled-components'
import {images} from '../../assets/images/images'
import {useSwitcher} from "../../hooks/useSwitcher";

type CommentProps = {
    columnIndex: number
    taskIndex: number
    commentIndex: number
    commentAuthor: string
    commentBody: string
    removeComment: (columnIndex: number, taskIndex: number, commentIndex: number) => void
    onEditComment: (e: ChangeEvent<HTMLTextAreaElement>, columnIndex: number, taskIndex: number, commentIndex: number) => void
}

export function Comment({
    columnIndex,
    taskIndex,
    commentIndex,
    commentAuthor,
    commentBody,
    removeComment,
    onEditComment,}: CommentProps) {

    const showTextArea = useSwitcher()

    return (
        <CommentWrapper>
            {showTextArea.isOn && <CommentTextAreaWrapper>
                <textarea
                    autoFocus={true}
                    value={commentBody}
                    onBlur={() => showTextArea.off()}
                    onChange={(e) => onEditComment(e, columnIndex, taskIndex, commentIndex)} />
            </CommentTextAreaWrapper>}
            <p onClick={() => showTextArea.on()}>{commentBody}</p>
            <CommentAuthorWrapper>
                {commentAuthor}:
            </CommentAuthorWrapper>
            <CommentRemoveButton onClick={() => removeComment(columnIndex, taskIndex, commentIndex)}/>
        </CommentWrapper>
    )
}

const CommentWrapper = styled.div`
  position: relative;
  padding: 4px 8px;
  margin-bottom: 8px;
  background: #e3e2e2;
  border-radius: 5px;
`

const CommentAuthorWrapper = styled.span`
  position: absolute;
  top: 3px;
  left: 8px;
  font-size: 12px !important;
  z-index: 10;
`

const CommentRemoveButton = styled.div`
  position: absolute;
  top: 15px;
  left: 94%;
  width: 15px;
  height: 15px;
  background: url(${images.trash}) center / cover no-repeat;
  cursor: grab;
`

const CommentTextAreaWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 15;
  
  & textarea {
    width: 100%;
    height: 100%;
  }
`