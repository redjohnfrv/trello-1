import React from 'react'
import {InputAddTask, ColumnInputEventChangeType} from '../../ui/Input'

export function ColumnInput({
    onChangeTaskPreview,
    columnInputValue,
    columnIndex}: ColumnInputEventChangeType) {

    return (
        <InputAddTask
            columnInputValue={columnInputValue}
            onChangeTaskPreview={onChangeTaskPreview}
            columnIndex={columnIndex} />
    )
}