import React, {ChangeEvent} from 'react'
import styled from 'styled-components'
import {variables} from '../../assets/styles/variables'
import {TaskPreview} from '../Task/TaskPreview'
import {ColumnInput} from './ColumnInput'
import {StateType} from '../../storage/initialState'

type BoardPreviewTaskProps = {
    data: StateType
    addTask: (inputValue: string, index: number) => void
    columnIndex: number
    removeTask: (columnIndex: number, taskIndex: number) => void
    columnInputValue: string
    onChangeTaskPreview: (e: ChangeEvent<HTMLInputElement>, index: number) => void
    showPreviewTaskInput: (columnIndex: number) => void
    changeTaskStatus: (e: ChangeEvent<HTMLSelectElement>, taskIndex: number, columnIndex: number) => void
    onChangeTaskDesc: (e: ChangeEvent<HTMLTextAreaElement>, taskIndex: number, columnIndex: number) => void
    onChangeComment: (e: ChangeEvent<HTMLInputElement>, taskIndex: number, columnIndex: number) => void
    addComment: (inputValue: string, columnIndex: number, taskIndex: number) => void
    removeComment: (columnIndex: number, taskIndex: number, commentIndex: number) => void
    showComment: (columnIndex: number, taskIndex: number) => void
    onEditComment: (e: ChangeEvent<HTMLTextAreaElement>, columnIndex: number, taskIndex: number, commentIndex: number) => void
    descriptionValueHandler: (columnIndex: number, taskIndex: number) => void
}

export function Column({
    data,
    addTask,
    columnIndex,
    removeTask,
    columnInputValue,
    onChangeTaskPreview,
    showPreviewTaskInput,
    changeTaskStatus,
    onChangeTaskDesc,
    onChangeComment,
    addComment,
    removeComment,
    showComment,
    onEditComment,
    descriptionValueHandler,}: BoardPreviewTaskProps ) {

    return (
        <ColumnPreviewTaskWrapper>
            <h2>{data.title}</h2>
            {data.todos.map((item, index) => {
                return <TaskPreview
                    key={index}
                    task={item}
                    taskIndex={index}
                    columnIndex={columnIndex}
                    removeTask={removeTask}
                    changeTaskStatus={changeTaskStatus}
                    onChangeTaskDesc={onChangeTaskDesc}
                    onChangeComment={onChangeComment}
                    addComment={addComment}
                    removeComment={removeComment}
                    showComment={showComment}
                    onEditComment={onEditComment}
                    descriptionValueHandler={descriptionValueHandler} />
            })}
            {data.showInput
             ? (
                 <>
                     <ColumnInput
                         columnInputValue={columnInputValue}
                         onChangeTaskPreview={onChangeTaskPreview}
                         columnIndex={columnIndex} />
                     <ColumnAddInputButton
                         onClick={() => addTask(columnInputValue, columnIndex)}>Add task
                     </ColumnAddInputButton>
                 </>
                )
             : null}
            <ShowInputButton onClick={() => showPreviewTaskInput(columnIndex)}>+</ShowInputButton>
        </ColumnPreviewTaskWrapper>
    )
}

const ColumnPreviewTaskWrapper = styled.div`
  width: 180px;
  min-width: 120px;
  min-height: 250px;
  margin-bottom: 10px;
  background: ${variables.lightGrey};
  border-radius: 15px;
  padding: ${variables.defaultPadding};
  text-align: center;
  
  &:not(:last-child) {
    margin-right: 10px;
  }
  
  & h2 {
    background: ${variables.theme};
    margin-bottom: 24px;
    padding: 8px;
    border-radius: 15px;
    font-style: italic;
  }
`

const ColumnAddInputButton = styled.span`
  display: inline-block;
  width: 100%;
  margin-top: 4px;
  font-style: italic;
  color: ${variables.defaultYellow};
  text-transform: uppercase;
  text-align: right;
  cursor: pointer;
  opacity: .6;
  
  &:hover {
    opacity: 1;
  }
`

export const ShowInputButton = styled.div`
  font-size: 56px;
  color: ${variables.defaultYellow};
  cursor: pointer;
  opacity: .6;
  
  &:hover {
    opacity: 1;
  }
`