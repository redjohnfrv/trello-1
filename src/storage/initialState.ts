import {DONE, IN_PROGRESS, TESTING, TO_DO} from '../constants/constants'

export type CommentType = {
    author: string
    body: string
}

export type TodoType = {
    author: string
    title: string
    desc: string
    comments: CommentType[]
    input: string
    showInput: boolean
}

export type StateType = {
    title: string
    todos: TodoType[]
    input: string
    showInput: boolean
}

export const initialState = [
    {
        title: TO_DO,
        todos: [],
        input: '',
        showInput: false,
    },
    {
        title: IN_PROGRESS,
        todos: [],
        input: '',
        showInput: false,
    },
    {
        title: TESTING,
        todos: [],
        input: '',
        showInput: false,
    },
    {
        title: DONE,
        todos: [],
        input: '',
        showInput: false,
    },
]