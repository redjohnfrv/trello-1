import React, {ChangeEvent} from 'react'
import styled from 'styled-components'

export type ColumnInputEventChangeType = {
    onChangeTaskPreview: (e: ChangeEvent<HTMLInputElement>, index: number) => void
    columnInputValue: string
    columnIndex: number
}

export type CommentInputEventChangeType = {
    taskIndex: number
    columnIndex: number
    commentInputValue: string
    onChangeComment: (e: ChangeEvent<HTMLInputElement>, taskIndex: number, columnIndex: number) => void
    addComment: (inputValue: string, columnIndex: number, taskIndex: number) => void
}

export function InputAddTask({
    onChangeTaskPreview,
    columnInputValue,
    columnIndex,}: ColumnInputEventChangeType) {

    return (
        <InputWrapper>
            <input
                autoFocus={true}
                type="text"
                value={columnInputValue}
                onChange={(e) => onChangeTaskPreview(e, columnIndex)}
                />
        </InputWrapper>
    )
}

export function InputAddComment({
    taskIndex,
    columnIndex,
    commentInputValue,
    onChangeComment,
    addComment,}: CommentInputEventChangeType) {
    return (
        <InputWrapper>
            <input
                autoFocus={true}
                type="text"
                value={commentInputValue}
                onChange={(e) =>
                    onChangeComment(e, taskIndex, columnIndex)}
                />
            <button onClick={() => addComment(commentInputValue, columnIndex, taskIndex)}>OK</button>
        </InputWrapper>
    )
}

const InputWrapper = styled.div`
  width: 100%;
  margin-top: 24px;
  
  & input {
    width: calc(100% - 10px);
    height: 36px;
    margin-bottom: 12px;
  }
`