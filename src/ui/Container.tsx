import React from 'react'
import styled from 'styled-components'
import {variables} from '../assets/styles/variables'

type ContainerProps = {
    children: React.ReactNode
}

export function Container({children}: ContainerProps) {
    return (
        <ContainerWrapper>
            {children}
        </ContainerWrapper>
    )
}

const ContainerWrapper = styled.div`
  box-sizing: border-box;
  width: 100%;
  max-width: 1280px;
  margin: 0 auto;
  padding: ${variables.defaultPadding};
  min-height: 100vh;
  background: ${variables.theme};
`