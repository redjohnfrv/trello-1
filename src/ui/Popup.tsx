import React from 'react'
import styled from 'styled-components'
import {variables} from '../assets/styles/variables'

type PopupProps = {
    children: React.ReactNode
    popupClose: () => void
}

export function Popup({children, popupClose}: PopupProps) {
    return (
        <PopupWrapper>
            <PopupContainer>
                <PopupClose onClick={popupClose}>
                   CLOSE
                </PopupClose>
                {children}
            </PopupContainer>
        </PopupWrapper>
    )
}

const PopupWrapper = styled.div.attrs({
    className: 'popup'
})`
  &.popup {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, .7);
    z-index: 10;
  }
`

const PopupContainer = styled.div.attrs({
    className: 'popup_container'
})`
    &.popup_container {
      position: relative;
      width: 600px;
      min-height: 50%;
      padding: 16px;
      padding-bottom: 30px;
      margin: 0 auto;
      margin-top: calc(40vh - 185px);
      background-color: ${variables.theme};
      border: 1px solid ${variables.defaultBlack};
    }
`

const PopupClose = styled.div.attrs({
    className: 'popup_close'
})`
    &.popup_close {
      position: absolute;
      top: 8px;
      left: calc(100% - 60px);
      width: 60px;
      color: ${variables.lightGrey};
      cursor: pointer;
    }
`
